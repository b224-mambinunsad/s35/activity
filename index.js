const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://Mambinunsad-224:Admin123@224-mambinunsad.uucqztz.mongodb.net/s35?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to MongoDB"));

// Schema

const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

// Model

const User = new mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if(result !== null && result.username == req.body.username){
			return res.send(`${req.body.username} already taken! Please make another username`)
		}else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return res.send("error")
				}else {
					return res.status(200).send("Successfully Registered")
				}
			})

		}
	})
});




app.listen(port, ()=> console.log(`Server running at port ${port}`))